--[[ 
 riven plugin test version by akjnj848.
 Do not share without my permission.
 if you want share to someone, please pm me(akjnj848).
 http://botoflegends.com/forum/user/8663-akjnj848/
 ]]

class 'Plugin'
if myHero.charName ~= "Riven" then return end

local Skills, Keys, Items, Data, Jungle, Helper, MyHero, Minions, Crosshair, Orbwalker = AutoCarry.Helper:GetClasses()
local qCount = 0
local lastQ = 0
local Rcast = 0
local allowQ = false
local allowAA = true
local Rup = false


function Plugin:__init()
	PrintChat("jRiven Loaded")
	SkillR = AutoCarry.Skills:NewSkill(false, _R, 900, "RivenFengShuiEngine", AutoCarry.SPELL_CONE, 60, false, false, 1.2, 234, 0, false)
end

function Plugin:OnTick()
  QReady = (myHero:CanUseSpell(_Q) == READY)
  WReady = (myHero:CanUseSpell(_W) == READY)
  EReady = (myHero:CanUseSpell(_E) == READY)
  RReady = (myHero:CanUseSpell(_R) == READY)
	IReady = (Ignite ~= nil and myHero:CanUseSpell(Ignite) == READY)

	Crosshair:SetSkillCrosshairRange(myHero.range + GetDistance(myHero.minBBox) + (self:GetQRadius()*2))
	target = Crosshair:GetTarget()
	if myHero:CanUseSpell(_Q) ~= READY and GetTickCount() > lastQ + 1000 then qCount = 0 end
	if Menu.Combo then self:Combo() end
end

function Plugin:Combo()
	if ValidTarget(target)then
		if QReady and qCount > 0 and (allowQ == true) then
			CastSpell(_Q, target.x, target.z)
		end
	end
end



function Plugin:GetQRadius()
	if Rup then
		if qCount == 2 then
			return 112.5
		else
			return 150
		end
	else
		if qCount == 2 then
			return 200
		else
			return 162.5
		end
	end
end

function Plugin.RegisterOnAttacked()
	if QReady and target thenMembersMembersFunctions
		if Menu.Combo then
		 CastSpell(_Q, target.x, target.z)
		end
	end
end

function Plugin:OnProcessSpell(unit, spell)
  if unit.isMe then PrintChat(spell.name) end
	if unit.isMe and spell.name == "RivenTriCleave" then
		lastQ = GetTickCount()
		myHero:MoveTo(mousePos.x, mousePos.z)
	end
	if unit.isMe and spell.name == "RivenFengShuiEngine" then
		Rcast = GetTickCount()
	end
	if spell.name:lower():find("attack") and spell.animationTime and QReady and Menu.Combo then
		end
end

function Plugin:OnAnimation(unit,animation)    
	if unit.isMe and animation:find("Spell1a") then 
		qCount = 1
	elseif unit.isMe and animation:find("Spell1b") then 
		qCount = 2
	elseif unit.isMe and animation:find("Spell1c") then 
		qCount = 3
	end
end

AdvancedCallback:bind('OnGainBuff', function(unit, buff)
		if buff.name == "RivenFengShuiEngine" then
		Rup = true
		end
		if buff.name == "riventricleavesoundone" then
		myHero:MoveTo(mousePos.x, mousePos.z)
		allowQ = false
		Orbwalker:ResetAttackTimer()
		end
		if buff.name == "riventricleavesoundtwo" then
		myHero:MoveTo(mousePos.x, mousePos.z)
		allowQ = false
		Orbwalker:ResetAttackTimer()
		end
		if buff.name == "riventricleavesoundthree" then
		myHero:MoveTo(mousePos.x, mousePos.z)
		allowQ = false
		Orbwalker:ResetAttackTimer()
		end

end)

AdvancedCallback:bind('OnLoseBuff', function(unit, buff)
	if unit.isMe then
		if buff.name == "rivenpassiveaaboost" then
		allowQ = true
		end
		if buff.name == "RivenFengShuiEngine" then
		Rup = false
		end
	end
end)

AutoCarry.Plugins:RegisterOnAttacked(Plugin.RegisterOnAttacked)

Menu = AutoCarry.Plugins:RegisterPlugin(Plugin(), "jRiven")
Menu:addParam("Combo", "Use Combo", SCRIPT_PARAM_ONKEYDOWN, true, GetKey("C"))
Menu:permaShow("Combo")